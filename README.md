# anomaly-detectives
Repo for the anomaly detection project of the fall 2018 Advanced Network Management course at Tsinghua

Overleaf link --> https://www.overleaf.com/5296399341dkfttxhhmtdw

Competition website: http://iops.ai/competition_detail/?competition_id=7&flag=1
- login: nathan.e.d.cornille@gmail.com
- pw: aiopsdetectives

Mendeley folder (for relevant articles): https://www.mendeley.com/community/anomaly-detectives/

Slack workspace --> https://anomalydetectives.slack.com/
 Final presentation --> https://docs.google.com/presentation/d/1HUj2uXjPr3fMcLHSK7N9ItwYn5SrQWkgqqO_2RDK_MU/edit#slide=id.p1
